import { ref } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";

export const useProductStore = defineStore("product", () => {
  const total = ref(0);
  const selectedProducts = ref(<Product[]>[]);
  const addProduct = (id: number, name: string, price: number): void => {
    selectedProducts.value.push({ id, name, price });
    total.value += price;
  };
  return { selectedProducts, addProduct, total };
});
